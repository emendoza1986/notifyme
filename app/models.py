from flask import current_app
from app import db, login_manager
from datetime import datetime
from flask_login import UserMixin

from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
''' Login_Manager '''
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

''' Create Classes '''
'''
Python Console
from app import db
db.create_all()
db.drop_all()
from app import User, Post
user_1 = User(email='asdf@gmail.com',password='password')
db.session.add(user_1)
db.session.commit()

User.query.all()
User.query.first()
User.query.filter_by(email='asdf@gmail.com').first()


'''
class User(db.Model, UserMixin):
    id = db.Column(db.Integer,primary_key=True)
    email = db.Column(db.String(120), unique=True, nullable=False)
    phone_sms = db.Column(db.String(120), unique=False, nullable=True)
    image_file = db.Column(db.String(20), unique=False, nullable=False, default='Default.png')
    password = db.Column(db.String(60),nullable=False)
    notifications = db.relationship('Notification', backref='superior', lazy=True)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    activation_code = db.Column(db.String(120), nullable=True)
    last_signin = db.Column(db.DateTime, nullable=True)

    def get_reset_token(self, expires_sec=1800):
        s = Serializer(current_app.config['SECRET_KEY'], expires_sec)
        return s.dumps({'user_id':self.id}).decode('utf-8')


    @staticmethod
    def verify_reset_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        return User.query.get(user_id)


    def __repr__(self):
        return str(self.email)

class Notification(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    #image_key = db.Column(db.String(20), unique=False, nullable=False) #do I want users to have more than one image?
    label = db.Column(db.String(120), nullable=False)
    message = db.Column(db.String(800), nullable=False)
    sms_alert = db.Column(db.Boolean, nullable=False)
    token = db.Column(db.String(120), nullable=False)
    display_image = db.Column(db.Boolean, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False,default=datetime.utcnow)

    alarm = db.Column(db.Boolean, nullable=False, default=False)
    alert_count =  db.Column(db.Integer, nullable=False, default=0)
    last_alert = db.Column(db.DateTime, nullable=True)

    def __repr__(self):
        return str(self.user_id)


class Data(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    total_users = db.Column(db.Integer, nullable=False, default=0)
    total_notifications = db.Column(db.Integer, nullable=False, default=0)
    total_alerts = db.Column(db.Integer, nullable=False, default=0)
    last_alert = db.Column(db.DateTime, nullable=True)
    last_signin = db.Column(db.DateTime, nullable=True)
    access_code = db.Column(db.String(20), nullable=False, default="Be still and know")

    def __repr__(self):
        return str(self.id)