function myFunction() {
  /* Get the text field */
  var copyText = document.getElementById("myInput");

  /* Select the text field */
  copyText.select();

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  alert("Copied HTML: " + copyText.value);
}

function changeUTCtoLocal(){
    var strDateTime = document.getElementById('last_viewed').innerHTML;
    var myUTC = new Date(strDateTime);
    var myDate = new Date(myUTC.toLocaleString() + ' UTC');
    document.getElementById("last_viewed").innerHTML = myDate.toLocaleString();
}

function changeAllUTCtoLocal(){
    var utc_date_list = document.getElementsByClassName("utc_date");
    var i;

    for (i = 0; i < utc_date_list.length; i++){

        var strDateTime = utc_date_list[i].innerHTML;
        if( strDateTime !== 'None') {
            var myUTC = new Date(strDateTime);
            var myDate = new Date(myUTC.toLocaleString() + ' UTC');
            utc_date_list[i].innerHTML = myDate.toLocaleString();
        }
    }
    something = 'notified.tk' +'@gm' +'ail.'+'com'
    document.getElementById("e9uefu9a").innerHTML = something
}
window.onload = changeAllUTCtoLocal;