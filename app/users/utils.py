import os, hashlib
from PIL import Image
from flask import  url_for, current_app
from app import mail
from flask_mail import Message


#todo delete old images
def save_image(form_image, image_size, email):
    size = {
        "0":50,
        "1":75,
        "2":100,
        "3":25
    }

    # random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_image.filename)
    # image_fn = random_hex + f_ext
    hash_object = hashlib.md5(email.encode())
    image_fn = hash_object.hexdigest() + f_ext
    image_path = os.path.join(current_app.root_path, 'static/profile', image_fn)

    output_size = (size[image_size], size[image_size])
    i = Image.open(form_image)
    i.thumbnail(output_size)
    i.save(image_path)

    return image_fn



def send_reset_email(user):
    token = user.get_reset_token()
    msg = Message('Password Reset Request', sender='dnetradingexperts@gmail.com', recipients=[user.email])
    msg.body = f'''To reset your password, visit the following link:
{url_for('reset_token', token=token, _external=True)}

If you did not make this request then simply ignore this email and no changes will be made.
'''
    mail.send(msg)

def send_phone_sms(user):
    msg = Message('Activation Code', sender='notified.tk@gmail.com', recipients=[user.phone_sms])
    msg.body = f'''
{user.activation_code}
'''
    mail.send(msg)

def sms_provider_list(input=None):
    provider_list_names = [
        ('0', 'Verizon'),
        ('1', 'AT&T'),
        ('2', 'T-Mobile'),
        ('3', 'Sprint'),
        ('4', 'US Cellular'),
        ('5', 'Boost Mobile'),
        ('6', 'Virgin Mobile'),
        ('7', 'Alltel'),
        ('8', 'Republic Wireless'),
    ]
    provider_list_ext = {
        '0':'@vtext.com',
        '1':'@txt.att.net',
        '2': '@tmomail.net',
        '3': '@messaging.sprintpcs.com',
        '4': '@email.uscc.net',
        '5': '@myboostmobile.com',
        '6': '@vmobl.com',
        '7': '@message.alltel.com',
        '8': '@text.republicwireless.com',
    }
    if input is None:
        return provider_list_names
    else:
        return provider_list_ext[input]