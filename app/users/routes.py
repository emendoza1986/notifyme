import secrets
from flask import render_template, url_for, flash, redirect, request, abort,Blueprint
from app.users.forms import RegistrationForm, LoginForm,UpdateAccountForm, RequestResetForm, ResetPasswordForm, PhoneSMSForm, PhoneSMSTokenForm, UpdateEmailForm, UpdatePasswordForm
from app import db, bcrypt
from app.models import User, Notification, Data
from flask_login import login_user, current_user, logout_user, login_required
from datetime import datetime
from app.users.utils import save_image, send_reset_email, sms_provider_list, send_phone_sms

users = Blueprint('users', __name__)


@users.route("/register", methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('main.home'))

    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(email=form.email.data, password=hashed_password)
        db.session.add(user)
        data = Data.query.get(1)
        data.total_users = data.total_users + 1
        db.session.commit()
        flash(f'Account created for {form.email.data}!', 'success') #bootstrap class
        return redirect(url_for('users.login'))
    return render_template('register.html', title='Register', form=form)




@users.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('main.home'))

    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')
            user.last_signin = datetime.utcnow()
            return redirect(next_page) if next_page else redirect(url_for('main.home'))
        else:
            flash('Login Unsuccessful. Please check username and password', 'danger')
    return render_template('login.html', title='Login', form=form)

@users.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('main.home'))


@users.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r

@users.route("/account", methods=['GET', 'POST'])
@login_required
def account():
    form = UpdateAccountForm()
    if form.validate_on_submit():
        if form.image.data:
            if bcrypt.check_password_hash(current_user.password, form.password.data):
                image_file = save_image(form.image.data, form.image_size.data, current_user.email)
                current_user.image_file = image_file
                db.session.commit()
                flash('Image successfully updated.', 'success')
            else:
                flash('Invalid Password.', 'warning')
        return redirect(url_for('users.account'))

    elif request.method == 'GET':
        form.email.data = current_user.email
        if current_user.activation_code is None:
            form.phone_sms.data = current_user.phone_sms
    image_file = url_for('static', filename='profile/' + current_user.image_file)
    return render_template('account.html', title='Account', image_file=image_file, form=form)

@users.route("/update_email", methods=['GET', 'POST'])
@login_required
def update_email():
    form = UpdateEmailForm()
    if form.validate_on_submit():
        if bcrypt.check_password_hash(current_user.password, form.password.data):
            current_user.email = form.email.data
            db.session.commit()
            flash(f'Your email has been updated!', 'success')
        else:
            flash('Invalid Password.', 'warning')
            return redirect(url_for('users.update_email'))

        return redirect(url_for('users.account'))
    return render_template('update_email.html', title='Update Email', form=form)

@users.route("/update_password", methods=['GET', 'POST'])
@login_required
def update_password():
    form = UpdatePasswordForm()
    if form.validate_on_submit():
        if bcrypt.check_password_hash(current_user.password, form.current_password.data):
            hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
            current_user.password = hashed_password
            db.session.commit()
            flash(f'Your password has been updated!', 'success') #bootstrap class
        else:
            flash('Current password invalid.', 'warning')
            return redirect(url_for('users.update_password'))

        return redirect(url_for('users.account'))
    return render_template('update_password.html', title='Update Password', form=form)


@users.route("/admin/user/<string:email>")
@login_required
def admin_user_notifications(email):
    if current_user.id is 1:
        page = request.args.get('page',1,type=int)
        user = User.query.filter_by(email=email).first_or_404()
        notifications = Notification.query.filter_by(superior=user).order_by(Notification.created_at.desc()).paginate(page=page,per_page=4)
        return render_template('admin_user_notifications.html', notifications=notifications,user=user)
    else:
        return render_template('404.html'), 404

#todo fix
@users.route("/admin/users")
@login_required
def admin_users():
    if current_user.id is 1:
        users = User.query.order_by(User.created_at.desc()).all()
        return render_template('admin_users.html',users=users)
    else:
        return render_template('404.html'), 404


@users.route("/reset_password", methods=['GET', 'POST'])
def reset_request():
    if current_user.is_authenticated:
        return redirect(url_for('main.home'))
    form = RequestResetForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        send_reset_email(user)
        flash('An email has been sent with instructions to reset your password.', 'info')
        return redirect(url_for('users.login'))
    return render_template('reset_request.html', title='Reset Password', form=form)

@users.route("/reset_password/<token>", methods=['GET', 'POST'])
def reset_token(token):
    if current_user.is_authenticated:
        return redirect(url_for('main.home'))
    user = User.verify_reset_token(token)
    if User is None:
        flash('Token is invalid or expired.', 'warning')
        return redirect(url_for('users.reset_request'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user.password = hashed_password
        db.session.commit()
        flash(f'Your password has been updated! You are now able to log in', 'success') #bootstrap class
        return redirect(url_for('users.login'))
    return render_template('reset_token.html', title='Reset Password', form=form)


#==============================
@users.route("/phone_sms", methods=['GET', 'POST'])
@login_required
def phone_sms_setup():
    form = PhoneSMSForm()
    if form.validate_on_submit():
        current_user.phone_sms = form.number.data + sms_provider_list(form.carrier.data)
        current_user.activation_code = secrets.token_urlsafe(5)
        db.session.commit()
        send_phone_sms(current_user)
        flash('An activation code has been sent to your phone.', 'info')
        return redirect(url_for('users.phone_sms_activate'))
    return render_template('phone_sms.html', title='Phone Text Notification', form=form)

@users.route("/phone_sms/activate", methods=['GET', 'POST'])
@login_required
def phone_sms_activate():
    if current_user.activation_code is None:
        return redirect(url_for('users.phone_sms_setup'))
    form = PhoneSMSTokenForm()
    if form.validate_on_submit():
        if current_user.activation_code != form.number.data:
            flash('The activation code was invalid or mistyped. Please try again.', 'warning')
            return redirect(url_for('users.phone_sms_activate'))

        current_user.activation_code = None
        db.session.commit()
        flash(f'Your phone has been successfully added!', 'success') #bootstrap class
        return redirect(url_for('users.account'))
    return render_template('phone_sms.html', title='Activate Text Notification', form=form)

