from flask_wtf import FlaskForm
from flask_login import current_user
from wtforms import StringField, PasswordField, SubmitField, BooleanField, SelectField, IntegerField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError, AnyOf, Regexp
from app.models import User, Data
from flask_wtf.file import FileField, FileAllowed
from app.users.utils import sms_provider_list
from app import bcrypt


class RegistrationForm(FlaskForm):
    #username = StringField('Username', validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password',
                                     validators=[DataRequired(), EqualTo('password')])
    access_code = StringField('Access Code', validators=[DataRequired()])
    submit = SubmitField('Sign Up')

    def validate_email(self,email):

        email = User.query.filter_by(email=email.data).first()
        if email:
            raise ValidationError('That email already exists in our database.')

    def validate_access_code(self, access_code):
        server = Data.query.get(1)
        if access_code.data != server.access_code:
            raise ValidationError('Invalid Access Code')

class LoginForm(FlaskForm):
    email = StringField('Email',render_kw={'autofocus':'True'},
                        validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')


class UpdateAccountForm(FlaskForm):
    email = StringField('Email', render_kw={'disabled':'True'})
    phone_sms = StringField('Text Notification', render_kw={'disabled':'True'})
    password = PasswordField('Password', validators=[DataRequired()])
    image = FileField('Update Email Image', validators=[FileAllowed(['jpg', 'png'])])
    image_size = SelectField('Image Size (pixels)', choices=[('0', '50x50 (default)'), ('1', '75x75'),('2', '100x100'),('3', '25x25')],
                            validators=[DataRequired(), AnyOf(['0', '1','2','3'])])
    submit = SubmitField('Update')


class UpdateEmailForm(FlaskForm):
    legend = StringField('Update Email')
    email = StringField('New Email', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Update')

    def validate_email(self,email):
        if email.data != current_user.email:
            email = User.query.filter_by(email=email.data).first()
            if email:
                raise ValidationError('That email already exists in our database.')

class UpdatePasswordForm(FlaskForm):
    legend = StringField('Update Password')
    current_password = PasswordField('Current Password', validators=[DataRequired()])
    password = PasswordField('New Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password',
                                     validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Update')


class RequestResetForm(FlaskForm):
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    submit = SubmitField('Request Password Reset')

    def validate_email(self,email):
        email = User.query.filter_by(email=email.data).first()
        if email is None:
            raise ValidationError('There is no account with that email. You must register first.')

class ResetPasswordForm(FlaskForm):

    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password',
                                     validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Reset Password')

class PhoneSMSForm(FlaskForm):
    legend = StringField('Activate Text Notification')
    number = StringField('10 Digit Phone Number', validators=[DataRequired(), Regexp('^[0-9]{10}$', message='10 digit phone number required')])
    carrier = SelectField('Carrier', choices=sms_provider_list(), validators=[DataRequired(), Regexp('^[0-8]{1}$', message='Only values between 0-8 allowed.')])
    submit = SubmitField('Send Activation Code')

class PhoneSMSTokenForm(FlaskForm):
    legend = StringField('Activate Text Notification')
    number = StringField('Activation Code', validators=[DataRequired()])
    submit = SubmitField('Add Phone Number')