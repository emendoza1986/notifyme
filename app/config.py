import json, os
with open(os.path.join(os.getcwd(),'env.json')) as env_json:
    env = json.load(env_json)

class Config:

    SECRET_KEY = env.get('secret')
    SQLALCHEMY_DATABASE_URI = env.get('database')
    ''' Flask Mail '''
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USERNAME = env.get('username')
    MAIL_PASSWORD = env.get('password')