from flask import Blueprint,current_app
from flask import render_template, request
from app.models import Notification, Data
from flask_login import login_user, current_user, logout_user, login_required

main = Blueprint('main', __name__)

# #todo disable cache
# @current_app.after_request
# def add_header(r):
#     """
#     Add headers to both force latest IE rendering engine or Chrome Frame,
#     and also to cache the rendered page for 10 minutes.
#     """
#     r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
#     r.headers["Pragma"] = "no-cache"
#     r.headers["Expires"] = "0"
#     r.headers['Cache-Control'] = 'public, max-age=0'
#     return r


@main.route("/")
@main.route("/home")
#todo modify home to not use current user data
def home():
    notifications = []
    if current_user.is_authenticated:
        page = request.args.get('page', 1, type=int)
        notifications = Notification.query.filter_by(user_id=current_user.id).order_by(Notification.created_at.desc()).paginate(page=page,per_page=4)

    return render_template('home.html', title='Home', notifications=notifications)

@main.route("/statistics")
def statistics():
    data = Data.query.get(1)
    return render_template('statistics.html',title='Statistics', data=data)

@main.route("/about")
def about():
    return render_template('about.html', title='About')

#todo howto
@main.route("/howto")
def howto():
    return render_template('howto.html', title='How To')

#todo contact
@main.route("/contact")
def contact():
    return render_template('contact.html', title='Contact')