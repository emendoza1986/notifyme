from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectField, TextAreaField
from wtforms.validators import DataRequired, AnyOf

class NotificationFormCreate(FlaskForm):
    legend = StringField('New Notification')
    label = StringField('Subject', validators=[DataRequired()])
    message = TextAreaField('Message', validators=[DataRequired()])
    sms_alert = SelectField('Notify via', choices=[('0', 'e-mail (default)'), ('1', 'text message (if set)')],
                            validators=[DataRequired(), AnyOf(['0', '1'])])
    display_image = SelectField('Display image', choices=[('0', 'hidden image (default)'), ('1', 'custom image (if set)')],
                            validators=[DataRequired(), AnyOf(['0', '1'])])
    submit = SubmitField('Create Notification')

class NotificationFormUpdate(FlaskForm):
    legend = StringField('Update Notification')
    label = StringField('Subject', validators=[DataRequired()])
    message = TextAreaField('Message', validators=[DataRequired()])
    sms_alert = SelectField('Notify via', choices=[('0', 'e-mail (default)'), ('1', 'text message (if set)')], validators=[DataRequired(), AnyOf(['0','1'])])
    submit = SubmitField('Update Notification')
