
from flask import  url_for
from app import mail
from flask_mail import Message

from threading import Thread
from app import mail_app


def send_async_email_alert(msg):

    with mail_app().app_context():
        mail.send(msg)


def send_alert_email(email, subject, message):
    msg = Message(subject, sender='notified.tk@gmail.com', recipients=[email])
    msg.body = f'''{message}'''

    thr = Thread(target=send_async_email_alert, args=[msg])
    thr.start()




def sms_alert_to_bool(integer, current_user):
    if current_user.activation_code is None and current_user.phone_sms is not None:
        return (True if integer == '1' else False)
    return False

def display_image_to_bool(integer, current_user):
    if current_user.image_file != 'Default.png':
        return (True if integer == '1' else False)
    return False

def destination(user, notification):
    return (user.phone_sms if notification.sms_alert else user.email)

def use_image(user, notification):
    return (user.image_file if notification.display_image else 'Default.png')

