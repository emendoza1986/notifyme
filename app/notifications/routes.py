from flask import Blueprint, render_template, url_for, flash, redirect, request, abort, send_from_directory, send_file
from app.notifications.forms import NotificationFormCreate, NotificationFormUpdate
from app import db
from app.models import Notification, User, Data
from flask_login import current_user, login_required
from datetime import datetime
import os, secrets
from app.notifications.utils import send_alert_email, sms_alert_to_bool, display_image_to_bool, destination,use_image


notifications = Blueprint('notifications', __name__)

STATIC_FILE_PATH = os.path.join('static','profile')

@notifications.route("/home/notification/new", methods=['GET', 'POST'])
@login_required
def new_notification():
    form = NotificationFormCreate()
    if form.validate_on_submit():
        token = secrets.token_urlsafe(4)
        notification = Notification(label=form.label.data, message=form.message.data, sms_alert=sms_alert_to_bool(form.sms_alert.data, current_user),superior=current_user, token=token, display_image=display_image_to_bool(form.display_image.data, current_user))
        db.session.add(notification)
        data = Data.query.get(1)
        data.total_notifications = data.total_notifications + 1
        db.session.commit()
        flash('Notification successfully created.', 'success')

        return redirect(url_for('notifications.notification', notification_id=notification.id))#return redirect(url_for('main.home'))
    return render_template('create_notification.html', title='Create Notification', form=form)




@notifications.route("/home/notification/<int:notification_id>")
@login_required
def notification(notification_id):
    notification = Notification.query.get_or_404(notification_id)
    if notification.superior == current_user:
        return render_template('notification.html', title=notification.label, notification=notification)

    return redirect(url_for('main.home'))



@notifications.route("/home/notification/<int:notification_id>/update", methods=['GET', 'POST'])
@login_required
def update_notification(notification_id):
    notification = Notification.query.get_or_404(notification_id)
    if notification.superior != current_user:
        return redirect(url_for('main.home'))
    form = NotificationFormUpdate()
    if form.validate_on_submit():
        notification.label = form.label.data
        notification.message = form.message.data
        notification.sms_alert = sms_alert_to_bool(form.sms_alert.data, current_user)
        # notification.display_image = display_image_to_bool(form.display_image.data, current_user)
        db.session.commit()
        flash('Your Notification has been updated.', 'success')
        return redirect(url_for('notifications.notification', notification_id=notification.id))
    elif request.method == 'GET':
        form.label.data = notification.label
        form.message.data = notification.message
        form.sms_alert.data = notification.sms_alert
        # form.display_image.data = notification.display_image
    return render_template('create_notification.html', title='Update Notification',
                           form=form, legend='Update Notification')


@notifications.route("/home/notification/<int:notification_id>/delete", methods=['POST'])
@login_required
def delete_notification(notification_id):
    notification = Notification.query.get_or_404(notification_id)
    if notification.superior != current_user:
        return redirect(url_for('main.home'))
    db.session.delete(notification)
    db.session.commit()
    flash('Your Notification has been deleted.', 'success')
    return redirect(url_for('main.home'))

@notifications.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r

@notifications.route("/<int:notification_id>/<string:token>")
def alert(notification_id, token):
    notification = Notification.query.get(notification_id)
    if notification:
        if notification.token == token:
            user = User.query.get(notification.user_id)
            if notification.alarm == True:
                notification.alert_count = notification.alert_count + 1
                notification.last_alert = datetime.utcnow()
                data = Data.query.get(1)
                data.total_alerts = data.total_alerts + 1
                data.last_alert = notification.last_alert
                db.session.commit()
                note_dest = destination(user, notification)
                send_alert_email(note_dest, notification.label, notification.message)
                return send_from_directory(STATIC_FILE_PATH, use_image(user,notification))
            else:# notification.alarm == False
                notification.alarm = True
                db.session.commit()
                return send_from_directory(STATIC_FILE_PATH, use_image(user,notification))

        return send_from_directory(STATIC_FILE_PATH, 'Default.png')